FROM python:3.8-slim AS base

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

FROM base AS python-deps

# install deps
RUN apt-get update && apt-get install -y --no-install-recommends gcc curl
RUN pip install pipenv
COPY Pipfile.lock .
COPY Pipfile .

# install libs
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

FROM python-deps AS runtime

COPY --from=python-deps /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

# create and switch to a new user
RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

COPY . .

ENTRYPOINT ["python", "bot.py"]