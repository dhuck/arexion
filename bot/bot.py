import discord
from discord.ext import commands
import configparser
import requests
import json, re
import urllib.parse
import logging
import os

logging.basicConfig(format='%(asctime)s : %(levelname)s >> %(message)s', level=logging.INFO)

config = configparser.ConfigParser()
config.read('config.ini')
print(config)

bot = commands.Bot(command_prefix=commands.when_mentioned_or("?"))
API_URL = os.environ['API_URL']
API_PORT = os.environ['API_PORT']


def parse_cmd(ctx):
    cmd = ctx.message.content.split()
    return cmd[1:]


@bot.event
async def on_ready():
    """On ready event!"""
    logging.info("Logged in as " + str(bot.user))
    logging.info("User ID: " + str(bot.user.id))
    logging.info("Ready!")


@bot.command()
async def ignore(ctx):
    """Manage Ignore"""
    logging.info("Got ignore request")
    cmd = parse_cmd(ctx)
    if not cmd:
        await ctx.send("Need a phrase to ignore")
        return
    phrase = ' '.join(cmd)
    safe_phrase = urllib.parse.quote(phrase)
    logging.info(phrase)
    res = requests.get(f'http://{API_URL}:{API_PORT}/ignore/{safe_phrase}')
    content = json.loads(res.content)
    await ctx.send(f"```{content['message']}```")


@bot.command()
async def follow(ctx):
    """Add users to the watchlist"""
    cmd = parse_cmd(ctx)
    if not cmd:
        return
    cmd = ''.join(cmd)
    cmd = urllib.parse.quote(cmd)
    res = requests.get(f'http://{API_URL}:{API_PORT}/follow/{cmd}')
    content = json.loads(res.content)
    logging.info(content)
    await ctx.send(f"```{content['message']}```")


@bot.command()
async def topic(ctx):
    """Add topic phrase to the watchlist"""
    cmd = parse_cmd(ctx)
    if not cmd:
        return
    cmd = ' '.join(cmd)
    cmd = urllib.parse.quote(cmd)
    res = requests.get(f'http://{API_URL}:{API_PORT}/topic/{cmd}')
    content = json.loads(res.content)
    await ctx.send(f"```{content['message']}```")


@bot.command()
async def ls(ctx):
    """lists different settings!
    -- ls ignore -> list the ignore list
    -- ls users --> list the users we are tracking
    -- ls topics -> list the different topics we are tracking
"""
    valid = ['user', 'users', 'topic', 'topics', 'ignore']
    logging.info("Got a list request")
    cmd = parse_cmd(ctx)
    if not cmd:
        return
    logging.info(cmd)
    if cmd[0] == 'users' or cmd[0] == 'topics':
        subject = re.sub('s', '', cmd[0])
    elif cmd[0] in valid:
        subject = cmd[0]
    else:
        await ctx.send(f"{cmd[0]} is not a valid subject")

    res = requests.get(f'http://{API_URL}:{API_PORT}/list/{subject}')
    content = json.loads(res.content)
    if res.ok:
        await ctx.send(f"```{subject.capitalize()} list: [{', '.join(content['result'])}]```")
    else:
        await ctx.send("Something went wrong :(")


@bot.command()
async def stats(ctx):
    """Database Stats"""
    res = requests.get(f'http://{API_URL}:{API_PORT}/count')
    content = json.loads(res.content)
    await ctx.send(
        f"I'm sitting on {content['unclassified_count']:,d} unclassified tweets and {content['classified_count']:,d} classified tweets, for a total of {content['total']:,d} tweets. I have removed {content['removed_count']:,d} duplicate tweets since last restart!")


@bot.command()
async def rm(ctx):
    """remove item from lists
    -- rm ignore -> remove phrase from the ignore list, comma separated
    -- rm users --> remove users from the user list, comma separated
    -- rm topic --> remove topics from the topic list, comma separated
"""
    logging.info("Got removal request!")
    cmd = parse_cmd(ctx)
    if not cmd:
        return
    phrase = ' '.join(cmd[1:])
    safe_phrase = urllib.parse.quote(phrase)
    res = None
    if cmd[0] == 'ignore':
        res = requests.get(f'http://{API_URL}:{API_PORT}/remove/ignore/{safe_phrase}')
    elif cmd[0] == 'users' or cmd[0] == 'user':
        res = requests.get(f'http://{API_URL}:{API_PORT}/remove/user/{safe_phrase}')
    elif cmd[0] == 'topic' or cmd[0] == 'topics':
        res = requests.get(f'http://{API_URL}:{API_PORT}/remove/topic/{safe_phrase}')
    else:
        await ctx.send("Not a valid list, type ?help if you big dumb.")
        return

    logging.info(res.status_code)
    if res and res.status_code == 200:
        content = json.loads(res.content)
        await ctx.send(f"```{content['message']}```")
    else:
        await ctx.send(f"Attention Bajoran Workers, something has happened.")


bot.run(config['DISCORD']['Token'])
