import threading
from flask import Flask, jsonify, send_file
from flask_pymongo import PyMongo
from flask_cors import CORS
from random import randint
from cryptobot import TwitterScraper
from pymongo.errors import DuplicateKeyError
import urllib.parse
from time import sleep
import re
from datetime import datetime as dt
import pprint
import os
import configparser
import logging
from queue import Queue
import json

config = configparser.ConfigParser()
config.read('config.ini')
pp = pprint.PrettyPrinter(indent=2)
logging.basicConfig(format='%(asctime)s : %(levelname)s >> %(message)s', level=logging.INFO)

server = os.environ['MONGO_SERVER']
user = os.environ['MONGO_USER']
pw = os.environ['MONGO_PASS']
port = os.environ['MONGO_PORT']
default_db = os.environ['MONGO_DB']

app = Flask(__name__)
app.config['MONGO_URI'] = f"mongodb://{user}:{pw}@{server}:{port}/{default_db}?authSource=admin"

USERS: list = list()
IGNORE_LIST: list = list()
TOPICS: list = list()
REMOVED_COUNT = 0


def setup_twitter(config: configparser.ConfigParser, q: Queue or None = None) -> TwitterScraper:
    logging.debug("Setting up Twitter...")
    auth: dict = dict()
    auth['consumer_key'] = config['TWITTER']['ConsumerKey']
    auth['consumer_secret'] = config['TWITTER']['ConsumerSecret']
    auth['access_token'] = config['TWITTER']['AccessToken']
    auth['access_token_secret'] = config['TWITTER']['AccessTokenSecret']

    return TwitterScraper(auth, q)


def clean_text(text):
    text = text.strip()
    for i in IGNORE_LIST:
        if i in text.lower():
            return False
    return text


def clean_tweet(tweet):
    if tweet['retweeted'] or tweet['lang'] != 'en':
        db.crypto_training_tweets.delete_one({'_id': tweet['_id']})
        return False

    out = dict()
    if tweet['truncated']:
        text = tweet['extended_tweet']['full_text']
    else:
        text = tweet['text']
    text = clean_text(text)
    if not text:
        db.crypto_training_tweets.delete_one({'_id': tweet['_id']})
        return False

    out['id'] = tweet['id_str']
    out['text'] = text
    return out


tweet_queue: Queue = Queue()

mongo = PyMongo(app)
db = mongo.db
twit: TwitterScraper = setup_twitter(config, tweet_queue)


def refresh_user():
    return [x['id'] for x in db.user_list.find()]


def refresh_ignore():
    return [x['phrase'] for x in db.ignore_list.find()]


def refresh_topics():
    return [x['phrase'] for x in db.topic_list.find()]


def add_tweet(tweet):
    try:
        db.crypto_training_tweets.insert_one(tweet)
    except DuplicateKeyError:
        pass


@app.route('/collect/<source>')
def collect(source):
    global IGNORE_LIST, USERS, TOPICS
    IGNORE_LIST = refresh_ignore()
    USERS = refresh_user()
    TOPICS = refresh_topics()
    source = source.lower()
    logging.debug(f"Source: {source}")
    if source == 'topics':
        logging.info("Watching topic list")
        twit.start_stream(filters=TOPICS, ignore=IGNORE_LIST)
    elif source == 'users':
        logging.info("Watching user list")
        twit.start_stream(users=USERS, ignore=IGNORE_LIST)
    elif source == 'stop':
        logging.info("Stopping collection...")
        twit.stop_stream()
    else:
        return jsonify({
            'error': 'not a valid collection source'
        })
    return jsonify({
        'message': f'collection {source}'
    })


@app.route('/ignore/<phrase>')
def ignore(phrase):
    global IGNORE_LIST
    phrases = urllib.parse.unquote(phrase)
    phrases = re.sub('"', '', phrases)
    phrases = re.sub("'", '', phrases)
    phrases = phrases.split(',')
    cleaned = [x.strip().lower() for x in phrases]
    added, skipped = list(), list()
    count = 0
    for p in cleaned:
        val = {'phrase': p}
        exists = list(db.ignore_list.find(val))
        if exists or len(p) < 5:
            skipped.append(p)
        else:
            count += 1
            db.ignore_list.insert_one(val)
            added.append(p)
    IGNORE_LIST = refresh_ignore()
    res = clean_ignore()
    res_count = json.loads(res.data)['count']
    logging.info(json.loads(res.data))
    message = []
    if added:
        message.append(f"{', '.join(added)} added to the topic list.")
    if skipped:
        message.append(f"{', '.join(skipped)} already present in topic list")
    message.append(f"Adding these terms resulted in {res_count} tweets being removed")
    return jsonify({
        'count': count,
        'message': '\n\n'.join(message)
    })


@app.route('/list/<subject>')
def list_subject(subject):
    if subject == 'ignore':
        res = db.ignore_list.find()
        res = [x['phrase'] for x in res]
    elif subject == 'topic':
        res = db.topic_list.find()
        res = [x['phrase'] for x in res]
    elif subject == 'user':
        res = db.user_list.find()
        res = [x['screen_name'] for x in res]
    else:
        logging.error("No such subject")
        res = []
    return jsonify({
        'count': len(res),
        'result': res
    })


@app.route('/prune')
def prune():
    global REMOVED_COUNT
    count = db.crypto_training_tweets.count_documents({})
    count = count * 3 // 5
    prune_sample = db.crypto_training_tweets.aggregate([
        {
            '$sample': {
                'size': count
            },

        }
    ],
        allowDiskUse=True
    )
    out = [db.crypto_training_tweets.delete_one({'_id': x['_id']}) for x in prune_sample]
    REMOVED_COUNT += len(out)
    return jsonify({
        'message': f"{len(out)} tweets pruned from database.",
        'count': len(out)
    })


def dataset_from_cursor(cur, src='unclassified'):
    if src == 'classified':
        logging.info("building classified set!")
        return [
            {
                'id': x['_id'],
                'user_id': x['user']['id'],
                'text': x['extended_tweet']['full_text'] if x['truncated'] else x['text'],
                'target': x['classification']
            }
            for x in cur
        ]
    else:
        return [
            {
                'id': x['_id'],
                'user_id': x['user']['id'],
                'text': x['extended_tweet']['full_text'] if x['truncated'] else x['text']
            }
            for x in cur
        ]


@app.route('/dump')
@app.route('/dump/<src>')
def dump(src='unclassified'):
    data = None
    count = 0
    if src == 'unclassified':
        res = db.crypto_training_tweets.find()
        data = dataset_from_cursor(res, src)
        count = len(data)
    else:
        return jsonify({
            "message": "nope"
        })
    filename = f"./datasets/tweets-dump-{src}-{count}-{dt.now().timestamp()}.json"
    with open(filename, 'w') as file:
        json.dump(data, file)

    return send_file(filename, as_attachment=True)


@app.route('/dataset')
@app.route('/dataset/<src>')
@app.route('/dataset/<src>/<count>')
def dataset(src='unclassified', count=10000):
    data = None
    count = int(count)
    message = 'success'
    filename = f"./datasets/tweets-{src}-{count}.json"
    if count >= 250_000 and os.path.exists(filename):
        return send_file(filename, as_attachment=True)
    query = None
    if src == 'unclassified':
        query = db.crypto_training_tweets.aggregate([
            {
                '$sample': {
                    'size': count
                }
            }
        ], allowDiskUse=True)
    elif src == 'classified':
        query = db.classified.aggregate([
            {
                '$sample': {
                    'size': count
                }
            }
        ], allowDiskUse=True)
    else:
        message = 'error! not a valid source'

    if query:
        data = dataset_from_cursor(query, src)
        count = len(data)

    filename = f"./datasets/tweets-{src}-{count}.json"
    with open(filename, 'w') as file:
        json.dump(data, file)

    return send_file(filename, as_attachment=True)


@app.route('/remove/<list_name>/<phrase>')
def remove(list_name, phrase):
    global USERS, IGNORE_LIST, TOPICS
    phrase = urllib.parse.unquote(phrase)
    phrase = re.sub('@', '', phrase)
    phrase = phrase.split(",")
    phrase = [x.strip().lower() for x in phrase]
    # logging.info(list_name, phrase)
    count = 0
    removed, not_found = list(), list()
    for p in phrase:
        p_ = re.compile(f"{p}", re.IGNORECASE)
        res = 0
        if list_name == 'ignore':
            val = {'phrase': {'$regex': p_}}
            res = db.ignore_list.delete_many(val)
            count += res.deleted_count
        elif list_name == 'user':
            val = {'screen_name': {'$regex': p_}}
            res = db.user_list.delete_many(val)
            count += res.deleted_count
        elif list_name == 'topic':
            val = {'phrase': p}
            res = db.topic_list.delete_many(val)
            count += res.deleted_count
        if res.deleted_count > 0:
            removed.append(p)
        else:
            not_found.append(p)

    if list_name == 'user':
        USERS = refresh_user()
    elif list_name == 'ignore':
        IGNORE_LIST = refresh_ignore()
    elif list_name == 'topic':
        TOPICS = refresh_topics()

    message = list()
    if removed:
        message.append(f"Removed {', '.join(removed)} from {list_name} list.")
    if not_found:
        message.append(f"Could not find {', '.join(not_found)}. Are you sure they exist?")
    return jsonify({
        'count': count,
        'message': '\n\n'.join(message)
    })


@app.route('/sample/<count>')
def sample_count(count=10):
    count = int(count)
    tweets = []
    while len(tweets) < count:
        sample = db.crypto_training_tweets.aggregate([{
            '$sample': {
                'size': count
            }
        }])

        for s in sample:
            out = clean_tweet(s)
            if out:
                tweets.append(out)

    return jsonify({
        'count': len(tweets),
        'tweets': tweets
    })


@app.route('/sample')
def sample():
    return sample_count(25)


@app.route('/clean/duplicates')
def remove_duplicate():
    global REMOVED_COUNT
    dupes_query = [
        {
            "$group": {
                "_id": {"clean_text": "$clean_text"},
                "uniqueIds": {"$addToSet": "$_id"},
                "count": {"$sum": 1}
            }
        },
        {
            "$match": {
                "count": {"$gt": 5}
            }
        },
        {
            "$sort": {
                "count": -1
            }
        }
    ]
    unc_dupes = db.crypto_training_tweets.aggregate(dupes_query, allowDiskUse=True)
    cl_dupes = db.classified.aggregate(dupes_query)
    dupes = list(unc_dupes)
    count = 0
    if len(dupes) > 0:
        ids = [id_ for x in dupes for id_ in x['uniqueIds']]
        count += len(ids)
        i, pager = 0, 2500
        while i + pager < len(ids):
            splice = ids[i:i + pager]
            db.crypto_training_tweets.delete_many({"_id": {"$in": splice}})
            i += pager
        db.crypto_training_tweets.delete_many({"_id": {"$in": ids[i:]}})
    dupes = list(cl_dupes)
    if len(dupes) > 0:
        ids = [id_ for x in dupes for id_ in x['uniqueIds']]
        count += len(ids)
        for _id in ids:
            db.classified.delete_one({"_id": _id})
    REMOVED_COUNT += count
    return jsonify({
        'count': count,
    })


@app.route('/classify/<_id>/<sentiment>')
def classify(_id, sentiment):
    tweet = db.crypto_training_tweets.find_one({"id_str": _id})
    tweet['text'] = clean_text(tweet['text'])
    tweet['classification'] = sentiment
    db.classified.insert_one(tweet)
    db.crypto_training_tweets.delete_one({"id_str": _id})
    return jsonify({
        'sentiment': sentiment,
        'id': _id,
        'tweet': tweet['id'],
        'tweetText': tweet['text']
    })


@app.route('/count')
def count_tweets():
    global REMOVED_COUNT
    c_count = db.classified.count_documents({})
    u_count = db.crypto_training_tweets.count_documents({})
    pp.pprint(c_count)
    return jsonify({
        'classified_count': c_count,
        'unclassified_count': u_count,
        'total': c_count + u_count,
        'removed_count': REMOVED_COUNT
    })


@app.route('/follow/<usernames>')
def follow(usernames):
    global USERS
    usernames = urllib.parse.unquote(usernames)
    usernames = re.sub('@', '', usernames)
    usernames = re.sub(' ', '', usernames)
    usernames = usernames.split(',')
    logging.info(usernames)
    added = []
    dupe = []
    not_found = []
    count = 0
    for u in usernames:
        user = twit.get_user(u)
        if user is None:
            not_found.append(u)
        else:
            user = user._json
            user['_id'] = user['id']
            try:
                db.user_list.insert_one(user)
                added.append(u)
                count += 1
            except DuplicateKeyError:
                dupe.append(u)
    message = []
    if added:
        message.append(f"{', '.join(added)} added to the follow list.")
    if dupe:
        message.append(f"{', '.join(dupe)} already in the follow list.")
    if not_found:
        message.append(f" {', '.join(not_found)} were not valid twitter usernames.")
    USERS = refresh_user()
    return jsonify({
        'count': count,
        'message': '\n\n'.join(message)
    })


@app.route('/topic/<topics>')
def topic(topics):
    global TOPICS
    topics = urllib.parse.unquote(topics)
    topics = re.sub('"', '', topics)
    topics = topics.split(',')
    cleaned = [x.strip().lower() for x in topics]
    added = []
    skipped = []
    count = 0
    for t in cleaned:
        val = {'phrase': t}
        exists = list(db.topic_list.find(val))
        if exists:
            skipped.append(t)
        else:
            count += 1
            db.topic_list.insert_one(val)
            added.append(t)
    message = []
    TOPICS = refresh_topics()
    if added:
        message.append(f"{', '.join(added)} added to the topic list.")
    if skipped:
        message.append(f"{', '.join(skipped)} already present in topic list")
    return jsonify({
        'count': count,
        'message': '\n\n'.join(message)
    })


@app.route('/delete/<_id>')
def delete(_id):
    db.crypto_training_tweets.delete_one({'_id': _id})


@app.route('/reset')
def reset():
    db.crypto_training_tweets.delete_many({})
    return jsonify({
        'message': 'success'
    })


@app.route('/clean/ignore')
def clean_ignore():
    global REMOVED_COUNT
    count = 0
    for p in IGNORE_LIST:
        search_text = re.compile(f"{p}", re.IGNORECASE)
        res = db.crypto_training_tweets.delete_many({'text': {'$regex': search_text}})
        count += res.deleted_count
    REMOVED_COUNT += count
    return jsonify({
        'count': count,
    })


def q_worker():
    logging.info("worker started")
    while True:
        if tweet_queue.empty():
            sleep(1)
        else:
            tweet = tweet_queue.get()
            add_tweet(tweet)
            if randint(0, 50) == 42:
                logging.debug(tweet['text'])


if __name__ == '__main__':
    logging.info("Starting Queue worker...")
    worker = threading.Thread(target=q_worker)
    USERS = refresh_user()
    IGNORE_LIST = refresh_ignore()
    TOPICS = refresh_topics()
    logging.info(f"USERS:       {USERS}")
    logging.info(f"IGNORE_LIST: {IGNORE_LIST}")
    logging.info(f"TOPICS:      {TOPICS}")
    worker.start()
    CORS(app)
    app.run(host="0.0.0.0", debug=True)
