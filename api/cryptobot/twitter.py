import tweepy
from tweepy import Stream
from re import sub
from pymongo.collection import Collection
from pymongo.errors import DuplicateKeyError
from tweepy.errors import NotFound, Forbidden
import logging
from random import randint
from queue import Queue


class TwitterScraper:
    def __init__(self, auth: dict, q: Queue = None):
        # Authentication Settings, tho
        self.__consumer_key = auth['consumer_key']
        self.__consumer_secret = auth['consumer_secret']
        self.__access_token = auth['access_token']
        self.__access_token_secret = auth['access_token_secret']

        # Authenticate with Twitter
        self.__auth = tweepy.AppAuthHandler(self.__consumer_key, self.__consumer_secret)
        self.__api = tweepy.API(self.__auth, wait_on_rate_limit=True)

        # Management
        self.__stream: Stream or None = None
        self.__queue: Queue = q if q else Queue()

        self.__user_ids: list or None = None

        # Stats
        self.__count = 0

    def pop_tweet(self):
        return self.__queue.get()

    def get_user(self, user_name):
        try:
            user_id = self.__api.get_user(screen_name=user_name)
            return user_id
        except NotFound:
            return None
        except Forbidden:
            return None

    def get_user_id(self, user_name):
        try:
            user_id = self.__api.get_user(screen_name=user_name)
            return user_id.id
        except NotFound:
            return -1
        except Forbidden:
            return -2

    def __get_user_ids(self, users: list):
        self.__user_ids = list()
        for user in users:
            try:
                user_id = self.__api.get_user(screen_name=user)
                user_id = user_id.id
                self.__user_ids.append(user_id)
            except NotFound:
                pass
            except Forbidden:
                pass

    def get_queue_size(self):
        return self.__queue.qsize()

    def start_stream(self, filters: list or None = None, users: list or None = None, ignore: list = []):
        if self.__stream is None:
            self.__stream = FireHose(self.__consumer_key, self.__consumer_secret,
                                     self.__access_token, self.__access_token_secret, self.__queue)

        self.__stream.filter(track=filters, follow=users, languages=['en'], threaded=True)

    def stop_stream(self):
        self.__stream.disconnect()
        del self.__stream
        self.__stream = None

    def watch_stream(self, filters: list):
        self.start_stream(filters)
        while True:
            tweet = self.pop_tweet()
            if tweet:
                logging.info(tweet.text)

    def tweet_sample(self) -> list:
        result = list()
        for tweet in tweepy.Cursor(self.__api.search_tweets, q="ethereum").items(10):
            result.append(tweet)
        return result


class FireHose(tweepy.Stream):

    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret, queue: Queue,
                 ignore: list = list()):
        super().__init__(consumer_key, consumer_secret, access_token, access_token_secret)
        self.__count = 0
        self.__queue: Queue = queue
        self.__ignore = ignore

    def on_status(self, status):
        obj = status._json
        if obj['truncated']:
            text = obj['extended_tweet']['full_text']
        else:
            text = obj['text']
        text = text.lower()
        if 'retweeted_status' in obj.keys():
            return
        if self.filter_tweet(text):
            obj['_id'] = obj['id_str']
            obj['clean_text'] = self.clean_tweet(text)
            self.__queue.put(obj)

    def filter_tweet(self, text):
        for phrase in self.__ignore:
            if phrase in text:
                return False
        return True

    @staticmethod
    def get_tweet_text(tweet: tweepy):
        if 'extended_tweet' in tweet:
            return tweet.extended_tweet['full_text']
        else:
            return tweet.text

    @staticmethod
    def clean_tweet(tweet):
        tweet = sub('#\S+\s', '', tweet)  # clean mentions and hashtags
        tweet = sub('@\S+\s', '', tweet)  # clean mentions
        tweet = sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', tweet)  # clean links
        tweet = sub(r'[\n\r]+|&amp;', ' ', tweet)  # newlines
        return tweet
