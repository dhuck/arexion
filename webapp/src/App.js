import {
  Box, Button, Card, CircularProgress, Container, createTheme, Fab, ThemeProvider, Typography
} from "@mui/material";
import '@fontsource/roboto-mono'
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import {useEffect, useState} from "react";
import Welcome from "./components/Welcome";

const axios = require('axios')


let apiUrl;
if (process.env.NODE_ENV === 'development' || !process.env.NODE_ENV) {
  apiUrl = 'localhost';
} else {
  apiUrl = 'arexion.tinycact.us';
}

const baseUrl = `http://${apiUrl}:8050/`;
const theme = createTheme({
  palette: {
    mode: 'dark',
  }, typography: {
    fontFamily: ["Roboto Mono"]
  }, components: {
    MuiButton: {
      defaultProps: {
        variant: "contained"
      }, styleOverrides: {
        root: {
          fontSize: '1.25rem',
        }
      }
    }
  }
})

function App() {
  const initState = {
    truncated: false, text: 'loading...'
  }

  let helloText = localStorage.getItem('hello-text');
  let initWelcome = !helloText || helloText === 'true';
  const [welcomeOpen, setWelcomeOpen] = useState(initWelcome);
  const [loading, setLoading] = useState(true)
  const [tweets, setTweets] = useState([])
  const [currTweet, setCurrTweet] = useState(initState)
  const [count, setCount] = useState(0)

  useEffect(() => {
    getMore(5)
  }, [])

  useEffect(() => {
    localStorage.setItem('hello-text', welcomeOpen)
  }, [welcomeOpen])

  useEffect(() => {
    setLoading(false);
  }, [tweets])

  const getMore = (count) => {
    axios.get(`${baseUrl}count`).then(r => {
      setCount(r.data.classified_count)
    }).catch(e => {
      console.log(e)
    })
    axios.get(`${baseUrl}sample/${count}`).then((r) => {
      let data = r.data.tweets;
      if (!tweets.length) {
        let tweet = data.pop()
        setCurrTweet(tweet);
      }
      setTweets([...tweets, ...data])
    }).catch(e => {
      console.log(e);
    })
  }

  const popTweet = () => {
    if (tweets.length < 20) {
      getMore(50)
    }
    if (tweets.length === 0) {
      setLoading(true);
      getMore(5);
      setCurrTweet(initState);
    } else {
      let data = tweets;
      let tweet = data.pop();
      setCurrTweet(tweet)
      setTweets(data)
    }
  }

  const buttonHandler = (sentiment) => {
    if (sentiment >= 0) {
      axios(`${baseUrl}classify/${currTweet.id}/${sentiment}`).then(r => {
        setCount(count + 1);
      }).catch(e => {
        setLoading(true);
        getMore(5);
        console.log(e)
      })
    } else {
      axios(`${baseUrl}remove/${currTweet.id}`).then(r => {
      }).catch(e => console.log(e))
    }
    popTweet();
  }

  if (loading) {
    return (<ThemeProvider theme={theme}>
        <CircularProgress size={64}/>
      </ThemeProvider>)
  } else {


    return (<ThemeProvider theme={theme}>
        <Welcome welcomeOpen={welcomeOpen} setWelcomeOpen={setWelcomeOpen}/>
        <div style={{
          backgroundColor: '#222',
        }}>
          <Container
            maxWidth='sm'
            sx={{
              backgroundColor: '#333', height: '100vh', overflow: 'auto',
            }}
          >
            <Box
              p={2}
              sx={{
                backgroundColor: theme.palette.background,
              }}>
              <Card
                variant='outlined'
                sx={{
                  height: '10em', padding: 2, fontSize: 20, marginBottom: 2, overflow: 'auto'
                }}>
                <Typography variant='body'>
                  {currTweet.text}
                </Typography>
              </Card>
              <Box
                sx={{
                  backgroundColor: theme.palette.background,
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'flex-start',
                }}
              >
                <Button
                  sx={{marginBottom: 1}}
                  onClick={() => buttonHandler(0)}
                  disabled={loading}
                >
                  Spam
                </Button>
                <Button
                  sx={{marginBottom: 1}}
                  onClick={() => buttonHandler(1)}
                  disabled={loading}
                >
                  Scam
                </Button>
                <Button
                  sx={{marginBottom: 1}}
                  onClick={() => buttonHandler(2)}
                  disabled={loading}
                >
                  Positive
                </Button>
                <Button
                  sx={{marginBottom: 1}}
                  onClick={() => buttonHandler(4)}
                  disabled={loading}
                >
                  Neutral
                </Button>
                <Button
                  sx={{marginBottom: 1}}
                  onClick={() => buttonHandler(3)}
                  disabled={loading}
                >
                  Negative
                </Button>
                <Button
                  color="secondary"
                  sx={{marginBottom: 1}}
                  onClick={() => buttonHandler(5)}
                  disabled={loading}
                >
                  Off Topic
                </Button>
                <Button
                  color="secondary"
                  sx={{marginBottom: 1}}
                  onClick={() => buttonHandler(-1)}
                  disabled={loading}
                >
                  Skip
                </Button>
              </Box>
              <Box p={2}>
                <Typography variant='body' sx={{color: '#ccc'}}>
                  {`${count} crypto brohs have been judged so far`}
                </Typography>
              </Box>
              <Fab
                sx={{
                  position: 'absolute', bottom: 25, right: 25
                }}
                onClick={() => setWelcomeOpen(true)}
              >
                <QuestionMarkIcon/>
              </Fab>
            </Box>
          </Container>
        </div>
      </ThemeProvider>);
  }
}

export default App;
