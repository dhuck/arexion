import {Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Typography} from "@mui/material";


export default function Welcome(props) {
  const {welcomeOpen, setWelcomeOpen, ...other} = props

  const handleClose = () => {
    setWelcomeOpen(false)
  }

  return (
    <Dialog open={welcomeOpen} maxWidth='sm'>
      <DialogTitle sx={{fontSize: '2.2rem', padding: 2}}>
        Classify Plz!
      </DialogTitle>
      <DialogContent style={{overflow: "auto"}}>
        <Box>
          <Typography variant="body">
            <p>Hello! Thank you for helping me classify these crypto brohs. The goal of this project is to create
              a neural network that can detect spam/scam tweets and classify normal tweets as having positive or
              negative sentiment, with a focus on cryptocurrency tweets.</p>

            <p>For the most part you should just choose what feels best to you. The more data we collect, the less any
              individual bias will be present in the training data. However, there are some hard and fast rules, I would
              you to keep in mind:</p>

                <p><b>Scam tweets follow a noticeable pattern.</b> If a tweet is asking for someone to DM them, or
                promising them some sort of money (usually round numbers), or quick money, it is a scam.</p>

                <p><b>Spam tweets are spammy.</b> You'll start to see a lot of of the same tweets or patterns and this will
                be easier to recognize over time. One good rule of thumb to follow is spamminess is proportional to the
                number of hashtags in the tweet. This does not necessarily follow with the number of mentions, since
                mentions could be part of a conversation.</p>

                <p><b>It's ok to make mistakes.</b> While accuracy in the data is important, it is better to have a lot of
                data with high accuracy, than a small amount of data with absolute accuracy. Don't spend too much time
                on any one tweet. If you make a mistake, don't sweat it, and classify more tweets.</p>

          </Typography>
        </Box>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleClose}
        >
          Get Started!
        </Button>
      </DialogActions>
    </Dialog>
  )
}